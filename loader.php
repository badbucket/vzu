<?php
try {
    require_once("include/functions.php");
    require_once("include/session.php");
    require_once("include/config.php");
    include("lang/".$lang."/common.php");
    include("lang/".$lang."/".$link.".php");
} catch (Exception $e) {
    $error = getError($e->getMessage());
    exit;
}
?>