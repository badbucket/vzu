 $(document).ready(function() {

     $("#navbar:visible>ul>li>a").each(function() {
         $(this).css({
             'padding-left': 0,
             'padding-right': 0,
             'width': $(this).outerWidth()
         });
     });

     $('#myCarousel').on('slide.bs.carousel', function(event) {
         if ($(event.relatedTarget).hasClass('item-skipped')) {
             event.preventDefault();
             $('#myCarousel').carousel(1);
         }
     });

     $('#myCarousel img[usemap]').each(function(index, element) {
         $(element).rwdImageMaps();
     });

     $(".hover-zoom-trigger").mouseenter(function() {
         $(this).find(".hover-zoom-container-triggered").addClass('hover');
     });

     $(".hover-zoom-trigger").mouseleave(function() {
         $(this).find(".hover-zoom-container-triggered").removeClass('hover');
     });

     function moveCarouselCaptions() {
         $('#carouselImageMap>area').each(function(index) {
             if (index > 0) {
                 var coor = $(this).attr('coords');
                 var coorA = coor.split(',');
                 var left = coorA[0];
                 var top = coorA[3] * 0.45;
                 var width = coorA[2] - coorA[0];
                 $($('#myCarousel>.carousel-inner>.item>.banner-caption-container')[index - 1]).css({
                     top: top + 'px',
                     left: left + 'px',
                     width: width
                 });
             }
         });
     }

     $(window).resize(function() {
         setTimeout(function() {
             moveCarouselCaptions();
         }, 250);
     });

     setTimeout(function() {
         moveCarouselCaptions();
         setTimeout(function() {
            $('#myCarousel>.carousel-inner>.item>.banner-caption-container').removeClass('invisible');
            $('ul.nav').removeClass('invisible');
         }, 100);
     }, 250);

    $('#contactform').submit(function() {
        $('#contactform button').attr('disabled','disabled');
        var email = $('#email').val();
        var subject = 'wpqr.cz - dotaz';
        var selection = $('#sel').val();
        var body = '<strong>Od: </strong>' + email + '<br><strong>Výběr: </strong>' + selection.toString() + '<br><strong>Zpráva: </strong><br>' + $('#message').val();
        $.post("../sendmail.php",
        {
            email: email,
            subject: subject,
            body: body
        },
        function(data, status){
            if (status === "success") {
                $('#contactformalert').addClass('alert-success');
                $('#contactformalert').html('<span class="glyphicon glyphicon-ok"></span> Vaše zpráva byla odeslána.');
                $('#email').val(null); 
                $('#sel').val(null); 
                $('#message').val(null); 
            }
            else {
                $('#contactformalert').addClass('alert-error');
                $('#contactformalert').html('<span class="glyphicon glyphicon-remove"></span> Něco se pokazilo, zkuste to ještě jednou.');
            }
            $('#contactformalert').removeClass('hide');
            $('#contactform button').removeAttr('disabled');
        });
        return false;
    });
    
 });
 