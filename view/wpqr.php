	<div class="container">
		<div class="row row-wpqr">
			<div class="row">
				<div class="col-sm-7">
					<h2><?php echo $lg['title_main']; ?></h2>
				</div>
				<div class="col-sm-5">
					<span class="link-to-download"><?php echo $lg['title_main_sub']; ?><a target="_blank" <?php echo 'href="../files/'.$lang.'/'.$lg['title_main_sub_link'].'"'; ?>> <?php echo $lg['title_main_sub_link_text']; ?></a></span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div notestyle="light_blue" class="wpqr-text-box wpqr-text-box-1 note-div" <?php if ($lang == 'de') echo 'style="height:178px;"'; ?>>
						<?php echo $lg['notebox_light_blue']; ?>
					</div>
					<div notestyle="green" class="wpqr-text-box wpqr-text-box-2 note-div" <?php if ($lang == 'de') echo 'style="height:169px;"'; ?>>
						<?php echo $lg['notebox_light_green']; ?>
					</div>
				</div>
				<div class="col-md-1 wpqr-arrows">
				</div>
				<div notestyle="dark_blue" class="col-md-7 wpqr-text-box wpqr-text-box-3 note-div" <?php if ($lang == 'de') echo 'style="height:355px;"'; ?>>
					<?php echo $lg['notebox_dark_blue']; ?>
				</div>
			</div>
		</div>
		<div class="row row-main">
			<div class="col-md-7">
				<?php echo $lg['row_main_text']; ?>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-4">
				<form id="contactform">
					<?php echo $lg['contact_form']; ?>
				</form>
				<div id="contactformalert" class="alert hide" role="alert"></div>
			</div>
		</div>
	</div>

	<hr class="hr-blue-new">
  	<div class="hr-blue-new-title" id="reference">
		  <span><?php echo $lg['reference_title']; ?></span>
		  <p class="text-center"><?php echo $lg['reference']; ?></p>
		  </div>
	
	<div class="container references-container">
		<?php echo $lg['reference_row']; ?>
	</div>

	<hr class="hr-blue-new">
  	<div class="hr-blue-new-title"><span><?php echo $lg['about_us_title']; ?></span></div>

	<div class="container about-us-container">
		<!--<div class="row about-us-row">-->
			<div class="row">
				<div class="col-xs-12" id="o-nas">
					<h2><?php echo $lg['company_name_header']; ?></h2>
				</div>
			</div>
			<div class="row row-eq-height">
				<div class="col-md-10" id="o-nas">
					<?php echo $lg['about_us']; ?>
					<!--<img class="img-circle img-mini hover-zoom" src="../img/o_nas_img_1.png" alt="" width="142" height="142">
					<img class="img-circle img-mini hover-zoom" src="../img/o_nas_img_2.png" alt="" width="142" height="142">-->
				</div>
				<div class="col-md-2 text-center hover-zoom-container logos-container">
					<img class="img-circle img-mini" src="../img/o_nas_img_1.png" alt="" width="135" height="135">
					<img class="img-circle img-mini" src="../img/o_nas_img_2.png" alt="" width="135" height="135">
				</div>
			</div>
		<!--</div>-->
		
		<?php echo $lg['images_row']; ?>

	</div>
