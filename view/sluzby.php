<div class="container">

	<div class="row hover-zoom-trigger">
		<div class="col-md-3 text-center hover-zoom-container-triggered"><img class="img-circle" src="../img/round_img_1.png" alt="" width="170" height="170"></div>
		<div class="col-md-9">
		<?php echo $lg['service_item_01']; ?>
		</div>
	</div>

	<div class="row hover-zoom-trigger" id="materialove-zkousky">
		<div class="col-md-3 text-center hover-zoom-container-triggered"><img class="img-circle" src="../img/round_img_2.png" alt="" width="170" height="170"></div>
		<div class="col-md-9">
		<?php echo $lg['service_item_02']; ?>
		</div>
	</div>

	<div class="row hover-zoom-trigger" id="zarove-nastriky">
		<div class="col-md-3 text-center hover-zoom-container-triggered"><img class="img-circle" src="../img/round_img_3.png" alt="" width="170" height="170"></div>
		<div class="col-md-9">
		<?php echo $lg['service_item_03']; ?>
		</div>
	</div>

	<div class="row hover-zoom-trigger">
		<div class="col-md-3 text-center hover-zoom-container-triggered"><img class="img-circle" src="../img/round_img_4.png" alt="" width="170" height="170"></div>
		<div class="col-md-9">
		<?php echo $lg['service_item_04']; ?>
		</div>
	</div>

	<div class="row hover-zoom-trigger">
		<div class="col-md-3 text-center hover-zoom-container-triggered"><img class="img-circle" src="../img/round_img_5.png" alt="" width="170" height="170"></div>
		<div class="col-md-9 services-container">
		<?php echo $lg['service_item_05']; ?>
		</div>
	</div>

	<div class="row hover-zoom-trigger">
		<div class="col-md-3 text-center hover-zoom-container-triggered"><img class="img-circle" src="../img/round_img_6.png" alt="" width="170" height="170"></div>
		<div class="col-md-9">
		<?php echo $lg['service_item_06']; ?>
		</div>
	</div>

	<div class="row hover-zoom-trigger">
		<div class="col-md-3 text-center hover-zoom-container-triggered"><img class="img-circle" src="../img/round_img_7.png" alt="" width="170" height="170"></div>
		<div class="col-md-9">
		<?php echo $lg['service_item_07']; ?>
		</div>
	</div>

	<div class="row hover-zoom-trigger">
		<div class="col-md-3 text-center hover-zoom-container-triggered"><img class="img-circle" src="../img/round_img_8.png" alt="" width="170" height="170"></div>
		<div class="col-md-9">
		<?php echo $lg['service_item_08']; ?>
		</div>
	</div>

</div>
