<?php
try {
    if (file_exists("modules/".$link.".php")) {
        include("modules/".$link.".php");
    }
    include("templates/header.php");
    include("view/".$link.".php");
    include("templates/footer.php");
} catch (Exception $e) {
    echo getError($e->getMessage());
    exit;
}
?>