<?php
$lg = array(
	'title' => 'Dienstleistungen',
	'text' => 'Zu unseren größten Abnehmer gehören diese Gesellschaften.',

	'service_item_01' => '
	<h2 style="padding-top: 15px">WPQR (maßgeschneidert) laut Norm EN ISO 15613 - 1</h2>
	<p>Wir übernehmen die komplette Ausstellung eines WPQR einschließlich Bemusterung, Prüfungen und Erstellung der Dokumentation. Den erforderlichen WPQR können Sie per E-Mail bestellen. Fertige WPQRs inklusive aller notwendigen Dokumente erhalten Sie innerhalb von 15 Werktagen ab Anlieferung des gewünschten Materials durch den Lieferanten. Die Originale aller Dokumente senden wir Ihnen per Post zu. <strong>Der gesamte WPQR-Erstellungsprozess wird von der deutschen Aufsichtsbehörde TÜV NORD überwacht. Die Unterlagen sind vollständig in deutscher Sprache (oder in einer Sprache Ihrer Wahl) abgefasst.</strong></p>',

	'service_item_02' => '
	<h2 style="padding-top: 15px">Materialtests - Mechanische Prüfungen, Metallographie, Chemie</h2>
	<p>Wir bieten umfassende Materialtests in akkreditierten Prüflaboren, einschließlich mechanischer Prüfungen (der Ermüdungs- und Creep-Eigenschaften), metallurgischer Analyse, chemischer und physikalischer Analyse von Metallen, Kunststoffen, Schmiermitteln usw. Wir bewerten die Restlebensdauer von Bauwerken und Anlagen, nehmen eine umfassende Analyse von Produktions- und Betriebsstörungen vor und erstellen Expertengutachten bezüglich der Materialkorrosion.</p>',

	'service_item_03' => '
	<h2 style="padding-top: 15px">Thermisches Spritzen</h2>
	<p>Wir applizieren Schutzschichten durch thermisches Spritzen (Hochgeschwindigkeitsspritzanlage HP / HVOF TAFA JP-5000, Lichtbogenspritzen mittels TAFA-Sprühpistole Modell 9000, Flammspritzen), kratz-, abrieb-, erosions-, korrosions und hitzebeständige, thermisch aufgespritzte Beschichtung auf Basis von Metallen, Legierungen, Superlegierungen, Cermets und Keramik. Durch thermisches Spritzen beschichten wir Komponenten mit einer Länge bis 4000 mm, einem Durchmesser bis 1200 mm und einem Gewicht bis 3200 kg. Wir entwickeln, beraten und führen tribologische Prüfungen von Materialien und Beschichtungen durch (Bestimmung der Abriebfestigkeit und der Adhäsionsbeständigkeit von Materialien).</p>',

	'service_item_04' => '
	<h2 style="padding-top: 15px">Wärmebehandlung</h2>
	<p>Wir beschäftigen uns mit der Entwicklung von Technologie für die thermische und chemisch-thermische Behandlung und Optimierung von Materialien sowie der Entwicklung von Fertigungstechnik. Wir führen Tests thermisch behandelter Metallmaterialien, Bearbeitung in Schutzatmosphäre und simulierte Wärmebehandlung auf einem SMITWELD-Simulator durch.</p>',

	'service_item_05' => '
	<h2 style="padding-top: 15px">Angewandte Mechanik</h2>
	<p>Wenn Sie ein Problem haben, helfen wir Ihnen, es zu lösen! Wir führen Berechnungen der Festigkeit, der Dehnung und Spannung in Strukturen durch. Wir verfügen über umfangreiche Erfahrungen mit der Analyse des dynamischen Verhaltens von Systemen – Multibody-Simulation. Wir lösen eine Vielzahl von Aufgaben, die mit der Strömung von Flüssigkeiten zusammenhängen, wie beispielsweise die externe und interne Aerodynamik von Fahrzeugen oder die Strömung in Kraftwerks-Anlagen.</p>',

	'service_item_06' => '
	<h2 style="padding-top: 15px">Dynamisches Prüflabor</h2>
	<p>Wir führen Tests der Festigkeit und der Ermüdungslebensdauer von Strukturen und ihren Teilen unter Simulation realer Betriebsbelastung mithilfe elektrohydraulischer Belastungssysteme (einschließlich der Simulation des Ansprechens auf Betriebsbelastung), Ermüdungsprüfungen von Konstruktionsmaterialien im Temperaturbereich bis 1000 °C und Tests seismischer Festigkeit und Stoßfestigkeit durch. Des Weiteren führen wir Falltests für Produkte, die labormäßige tensometrische Messung einachsiger und mehrachsiger Belastung, die tensometrische Messung der Belastung von Konstruktionen und Maschinen unter Betriebsbedingungen (einschließlich Messungen an rotierenden Maschinenteilen, im Magnetfeld, in korrosiver Umgebung, bei Temperaturen von -269 ° C bis 500 ° C) sowie die Messung technologischer und Restspannung durch.</p>',

	'service_item_07' => '
	<h2 style="padding-top: 15px">Lärm und Vibrationen</h2>
	<p>Wir messen Lärm und Vibrationen, identifizieren Quellen von Lärm und Vibrationen und schlagen Maßnahmen zu deren Reduktion vor (Maschinen und Anlagen, Transportmittel usw.). Wir führen Vibrodiagnostik an Turbinen, Generatoren, Getrieben, Pumpen usw. sowie betriebsbedingtes Auswuchten rotierender Maschinen durch. Wir prüfen die dynamischen Eigenschaften von Maschinenfundamenten und führen experimentelle Modalanalysen durch. Nicht zuletzt kalibrieren wir Schwingungssensoren und Messgeräte und werten die Ergebnisse seismischer Tests aus. Wir messen die frequenz-modalen Eigenschaften sich drehender Schaufeln sowie die Schallintensität.</p>',

	'service_item_08' => '
	<h2 style="padding-top: 15px">Kalibrierlabor</h2>
	<p>Wir kalibrieren in drei großen Laboratorien. Dies betrifft die Kalibrierung von Länge und Winkel, Schwingungen und Momenten sowie von Kraft.</p>',
);
?>