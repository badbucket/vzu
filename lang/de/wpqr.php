<?php
$lg = array(
	'title' => 'WPQR',
	'title_main' => 'WPQR-Erstellung (maßgeschneidert)<span> – WPQR nach Standard ISO 15613 - 1-14</span>',
	'title_main_sub' => 'Eine detaillierte Beschreibung der durchgeführten Arbeiten finden Sie ',
	'title_main_sub_link' => 'Bericht über die Qualifizierung des Schweißverfahrens (WPQR).pdf',
	'title_main_sub_link_text' => 'HIER (PDF)',
	'notebox_light_blue' =>    '<h3>WPQR-Bestellungper E-Mail</h3>
								<p><strong>Sie geben ein: </strong>Grund- und Zusatzmaterial, Art der Schweißnaht, Lage der Schweißnaht, Schweißtechnologie usw.</p>',
	'notebox_light_green' =>   '<h3>Sie erhalten einen WPQR.</h3>
								<p>Alle Dokumente erhalten Sie sowohl per Post (physisch) als auch in elektronischer Form (E-Mail). Es gibt eine umfassende Dokumentation in Deutsch oder Englisch.</p>',
	'notebox_dark_blue' =>  '<div>
								<p>Entsprechend der von Ihnen vorgegebenen Parameter besorgen wir den erforderlichen WPQR</p>
							</div>
							<div>
								<ol>
									<li>Wir erstellen eine pWPS (mehr)</li>
									<li>Wir sorgen für das erforderliche Grund- und Zusatzmaterial</li>
									<li>Wir sorgen für die Kontrolle durch: TÜV NORD (www.tuev-nord.de/)</li>
									<li>Wir bereiten die notwendigen Unterlagen vor</li>
									<li>Wir schweißen Testnähte</li>
									<li>Wir bewerten Schweißnähte: mittels nicht-destruktiver Tests (NDT), mechanischer und metallographischer Prüfungen</li>
									<li>Die Inspektionsstelle stellt die WPQR-Zertifikate aus</li>
								</ol>
							</div>',
	'row_main_text' => '<h2>Einen WPQR zu bekommen, war noch nie einfacher und günstiger.</h2>
						<p>Den gewünschten WPQR bestellen Sie auf elektronischem Wege per E-Mail. Den fertigen, für Ihr Unternehmen ausgestellten WPQR erhalten Sie per Post als auch per E-Mail. <strong>Der gesamte Prozess der WPQR-Erstellung wird von der internationalen Aufsichtsorganisation TÜV NORD beaufsichtigt. Alle Dokumente sind in deutscher oder englischer Sprache (je nach Wahl des Kunden).</strong></p>
						<h2>Maßgeschneiderter WPQR. Sie brauchen nichts zu tun. So sparen Sie Zeit und Geld.</h2>
						<p>Wir erstellen eine pWPS, prüfen Material und Schweißverbindungen, nehmen Inspektionen vor, und stellen abschließend einen WPQR aus. Sie sparen Arbeit, Zeit und Geld.</p>
						<h2>Sie schweißen die Testnaht selbst?</h2>
						<p>Ja, auch das ist möglich. Testschweißnähte können Sie auch fertig liefern (von Ihnen geschweißt). In diesem Fall sorgen wir für die notwendigen Materialprüfungen, die Aufsicht durch den TÜV NORD und den abschließenden WPQR.</p>',
	
	'contact_form' =>  '<h2 class="form-header">Kontakt-Formular</h2>
						<div class="form-group">
							<label for="sel">Wofür interessieren Sie sich?</label>
							<select multiple class="form-control" name="sel[]" id="sel" required>
								<option>Massgeschneiderter WPQR</option>
								<option>WPQR – Materialtests der Schweißverbindung</option>
								<option>Mechanische Prüfungen</option>
								<option>Matematische Analyse</option>
							</select>
						</div>
						<div class="form-group">
							<label for="message">Bitte Ihre Frage / Anfrage präzisieren</label>
							<textarea class="form-control" rows="2" name="message" id="message" placeholder="Ihr Text eingeben" required></textarea>
						</div>
						<div class="form-group">
							<label for="email">Ihr E-Mail</label>
							<input type="email" class="form-control" name="email" id="email" placeholder="Schreiben Sie Ihre E-Mail" required>
						</div>
						<button type="submit" class="btn btn-lg btn-block btn-orange">Frage absenden</button>',

	'reference_title' => 'Referenzen',
	'reference' => 'Wir verlassen uns auf 100% Zufriedenheit unserer Kunden.',
	'reference_row' => '<div class="row text-center hover-zoom-container">
							<div class="col-md-3 text-center">
								<img class="img-circle" src="../img/reference/reference_doosan.png" alt="" width="222" height="210">
								<div class="reference-text-box">
									<strong>DOOSAN</strong><br>
									Turbo Generator-System Hersteller
								</div>	
							</div>
							<div class="col-md-3">
								<img class="img-circle" src="../img/reference/reference_skoda.png" alt="" width="222" height="210">
								<div class="reference-text-box">
									<strong>ŠKODA TRANSPORTATION</strong><br>
									die Spitze im Bereich der Verkehrstechnik	
								</div>	
							</div>
							<div class="col-md-3">
								<img class="img-circle" src="../img/reference/reference_seele.png" alt="" width="222" height="210">
								<div class="reference-text-box">
									<strong>SEELE</strong><br>
									Weltweiter Hersteller von großen Schweißkonstruktionen
								</div>	
							</div>
							<div class="col-md-3">
								<img class="img-circle" src="../img/reference/reference_sanborn.png" alt="" width="222" height="210">
								<div class="reference-text-box">
									<strong>SANBORN</strong><br>
									<span style="font-size:11px;">Weltweiter Hersteller von Teilen für Strom-, Petrochemie- und Transportausrüstung</span>
								</div>
							</div>
						</div>',

	'about_us_title' => 'Über uns',
	
	'company_name_header' => 'Výzkumný a zkušební ústav Plzeň s.r.o. - 110 JAHRE (1907 - 2017)',	
	
	'about_us' =>  '<p>Das Forschungs- und Prüfungsinstitut Pilsen (Výzkumný a zkušební ústav <strong>Plzeň</strong> s.r.o.) wurde 1907 gegründet. Es beschäftigt sich mit Forschung und Entwicklung und führt akkreditierte Tests durch. Zu seinen wichtigsten Aktivitäten gehören:</p>
					<ul>
						<li>Forschung und Prüfung hinsichtlich der Erhöhung der Betriebssicherheit und der Lebensdauer von Energieanlagen – Vibrodiagnostik, Geräuschreduktion, Materialtests usw.</li>
						<li>Komplexe Lösung von Problemen im Zusammenhang mit betrieblicher Belastung, Zuverlässigkeit und Lebensdauer von Straßen- und Schienenfahrzeugen - Computersimulationen, Prüfungen im Labor, Messungen während des Betriebs.</li>
						<li>Akkreditierte Prüfungen, Kalibrierungen und Messungen für einen großen Kundenkreis. Prüfung und Analyse von Schweißverbindungen.</li>
						<li>Berechnungen im Bereich Festigkeit, Dynamik, Ermüdungsschäden, Deformationsbeständigkeit, Aerodynamik und Thermomechanik.</li>
						<li>Erforschung und Entwicklung thermischer Spritzbeschichtungen für die Primärproduktion und Renovierung, einschließlich industrieller Anwendungen.</li>
					</ul>',

	'images_row' => '<div class="row images-row hover-zoom-container">
						<div class="images-row-col-8">
							<div><p><strong>WPQR</strong><br>Material, Schweißen<br>Überwachung, Prüfung<br>Dokumentation</p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_1.png" alt="" width="114" height="114">
						</div>
						<div class="images-row-col-8">
							<div><p><strong>Materialtests</strong><br>Mechanische Prüfungen<br>Metallographie<br>Chemie</p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_2.png" alt="" width="114" height="114">
						</div>
						<div class="images-row-col-8">
							<div><p><strong>Wärmebehandlung</strong></p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_3.png" alt="" width="114" height="114">
						</div>
						<div class="images-row-col-8">
							<div><p><strong>Angewandte<br>Mechanik</strong></p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_4.png" alt="" width="114" height="114">
						</div>
						<div class="images-row-col-8">
							<div><p><strong>Thermisches<br>Anspritzen</strong></p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_5.png" alt="" width="114" height="114">
						</div>
						<div class="images-row-col-8">
							<div><p><strong>Dynamischer<br>Prüflabor</strong></p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_6.png" alt="" width="114" height="114">
						</div>
						<div class="images-row-col-8">
							<div><p><strong>Lärm und Vibrationen</strong></p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_7.png" alt="" width="114" height="114">
						</div>
						<div class="images-row-col-8">
							<div><p><strong>Kalibration</strong></p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_8.png" alt="" width="114" height="114">
						</div>
					 </div>'
	
);
?>