<?php
$common = array(
	
	'description' => 'Wir erstellen maßgeschneiderten Bericht über die Qualifizierung des Schweißverfahrens (WPQR) laut Standard ISO 15613. Wir erstellen pWPS, besorgen Material, nehmen Inspektionen vor, schweißen, prüfen, fassen die Dokumentation zusammen und übergeben Ihnen fertige WPQR in vereinbarten Sprache.',
	'keywords' => 'WPQR,maßgeschneiderter WPQR,pWPS,Schweißen,besorgen WPQR,fertige WPQR',

	'wpqr' => 'WPQR',
	'sluzby' => 'Materialprüfmaschine, Dienstleistungen und Flammschutzmittel Beschichtung',
	'reference' => 'REFERENZEN',
	'o-nas' => 'ÜBER UNS',
	'kontakt' => 'KONTAKTE',

	'banner_text_znak' => 'International zertifiziertes Labor',
	'banner_text_ruka' => 'Ausgezeichnetes Preis-Leistungs-Verhältnis',
	'banner_text_lide' => 'Geprüfte Kunden auf der ganzen Welt. Wir feiern 110 Jahre.'

);
?>