<?php
$common = array(

	'description' => 'Vypracování WPQR na klíč dle standardů ISO 15613. Vytvoříme pWPS, nakoupíme materiál, zajistíme dozor, svaříme, odzkoušíme, vytvoříme dokumentaci a předáme vám hotovou WPQR v požadovaném jazyce.',
	'keywords' => 'WPQR,WPQR na klíč,pWPS,svařování,koupím WPQR,hotová WPQR',

	'wpqr' => 'WPQR (na klíč)',
	'sluzby' => 'MATERIÁLOVÉ ZKOUŠKY, ŽÁROVÉ NÁSTŘIKY A DALŠÍ SLUŽBY',
	'reference' => 'REFERENCE',
	'o-nas' => 'O NÁS',
	'kontakt' => 'KONTAKT',

	'banner_text_znak' => 'Mezinárodně akreditovaná laboratoř',
	'banner_text_ruka' => 'Skvělý poměr kvalita/cena',
	'banner_text_lide' => 'Ověřeno zákazníky z celého světa. Slavíme 110 let.'

);
?>