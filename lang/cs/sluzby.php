<?php
$lg = array(
	'title' => 'Služby',
	'text' => 'Mezi naše odběratele patří i tyto společnosti.',

	'service_item_01' => '
	<h2 style="padding-top: 15px">WPQR (na klíč) dle normy EN ISO 15613 - 1</h2>
	<p>Provádíme kompletní vyhotovení WPQR včetně výroby vzorků, provedení zkoušek a vyhotovení dokumentace. Požadovanou WPQR si objednáte elektronicky prostřednictvím e-mailu. Hotové WPQR včetně všech potřebných dokumentů obdržíte do 15 pracovních dnů od chvíle, kdy nám dodavatel doručí požadovaný materiál. Originály veškerých dokumentů Vám zašleme poštou. <strong>Celý proces tvorby WPQR dozoruje německá dozorová organizace TÜV NORD. Dokumenty jsou kompletně v německém jazyce (nebo v jazyce, který si sami zvolíte.).</strong></p>',

	'service_item_02' => '
	<h2 style="padding-top: 15px">Materiálové zkoušky - Mechanické zkoušky, metalografie, chemie</h2>
	<p>Provádíme komplexní materiálové testy v akreditovaných zkušebních laboratořích zahrnující mechanické zkoušky (včetně testování únavových a creepových vlastností), metalurgickou analýzu a chemické a fyzikální rozbory kovů, plastů, maziv apod. Nabízíme posouzení zbytkové životnosti konstrukcí a zařízení, komplexní rozbory výrobních a provozních poruch a expertizy problematiky koroze materiálů.</p>',

	'service_item_03' => '
	<h2 style="padding-top: 15px">Žárové nástřiky</h2>
	<p>Poskytujeme aplikace ochranných povlaků metodami žárových nástřiků (vysokorychlostní nástřik HP/HVOF TAFA JP-5000, nástřik el. obloukem TAFA Model 9000, nástřik plamenem), žárově stříkané povlaky odolné proti otěru, abrazi, erozi, korozi, vysokým teplotám atd. na bázi kovů, slitin, superslitin, cermetů a keramiky. Žárové nástřiky provádíme na součástech o max. délce 4000 mm, max. průměru 1200 mm a max. hmotnosti 3200 kg. Provádíme vlastní vývoj, poskytujeme poradenství a nabízíme tribologické testování materiálů a povlaků (určování abrazivní a adhezivní odolnosti materiálů).</p>',

	'service_item_04' => '
	<h2 style="padding-top: 15px">Tepelné zpracování</h2>
	<p>Věnujeme se vývoji technologií tepelného, chemicko-tepelného zpracování a optimalizaci materiálů a technologií výroby. Provádíme zkoušky tepelného zpracování kovových materiálů, zpracování v ochranné atmosféře a simulace tepelného zpracování na zařízení Smitweld.</p>',

	'service_item_05' => '
	<h2 style="padding-top: 15px">Aplikovaná mechanika</h2>
	<p>Pokud máte nějaký problém, my Vám ho pomůžeme vyřešit! Provádíme pevnostní výpočty, výpočty deformací a napětí v konstrukcích. Máme bohaté zkušenosti s analýzou dynamického chování soustav – multibody simulace. Řešíme široké spektrum úloh, spojených s prouděním tekutin, například vnější a vnitřní aerodynamiku vozidel nebo proudění v energetických zařízeních.</p>',

	'service_item_06' => '
	<h2 style="padding-top: 15px">Dynamická zkušebna</h2>
	<p>Provádíme zkoušky pevnosti a únavové životnosti konstrukcí a jejich částí při simulaci reálného provozního zatížení pomocí elektrohydraulických zatěžovacích systémů (včetně simulace odezvy na provozní zatížení), únavové zkoušky konstrukčních materiálů v rozsahu teplot do 1000°C, zkoušky seismické odolnosti a otřesu-vzdornosti. Dále provádíme pádové zkoušky výrobků, laboratorní tenzometrická měření jednoosého i víceosého namáhání, tenzometrická měření namáhání konstrukcí a strojů v provozních podmínkách (včetně měření na rotujících částech strojů, v magnetickém poli, agresivním prostředí, při teplotách -269 °C až 500 °C)a měření technologických a zbytkových napětí.</p>',

	'service_item_07' => '
	<h2 style="padding-top: 15px">Hluk a vibrace</h2>
	<p>Zabýváme se měřením hluku a vibrací, identifikací zdrojů hluku a vibrací, návrhy na snížení vibrací a protihluková opatření (stroje a zařízení, dopravní prostředky, atd.). Nabízíme vibrodiagnostiku turbín, generátorů, převodovek, čerpadel, atd. a provozní vyvažování rotačních strojů. Kontrolujeme dynamické vlastnosti základů strojních zařízení a provádíme experimentální modální analýzu. V neposlední řadě provádíme také kalibraci snímačů vibrací a měřicích aparatur a vyhodnocování výsledků seismických zkoušek, měření frekvenčně modálních vlastností lopatek za rotace a měření akustické intenzity.</p>',

	'service_item_08' => '
	<h2 style="padding-top: 15px">Kalibrační laboratoř</h2>
	<p>Poskytujeme kalibrace spadající pod tři hlavní kalibrační laboratoře. Jedná se o kalibrace délky a úhlu, kalibrace vibrací a momentů a kalibrace síly. </p>',

);
?>