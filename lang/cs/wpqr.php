<?php
$lg = array(
	'title' => 'WPQR',
	'title_main' => 'Zajištění WPQR (na klíč)<span> – WPQR dle standardů ISO 15613 – 1 až 14</span>',
	'title_main_sub' => 'Podrobný popis prováděných prací naleznete ',
	'title_main_sub_link' => 'Postup při kvalifikaci WPQR.pdf',
	'title_main_sub_link_text' => 'ZDE (PDF)',
	'notebox_light_blue' =>    '<h3>E-mailem objedná WPQR</h3>
								<p><strong>Zadáte: </strong>základní a přídavný materiál, typ sv., polohu sv., technologii atd.</p>',
	'notebox_light_green' =>   '<h3>Obdržíte WPQR.</h3>
								<p>Dokumenty obdržíte poštou (fyzicky) a v elektronické podobě (E-mailem). Dokumentace v Německém nebo Anglickém jazyce.</p>',
	'notebox_dark_blue' =>  '<div>
								<p>Dle Vámi zaslaných parametrů zajistíme požadovanou WPQR</p>
							</div>
							<div>
								<ol>
									<li>Vytvoříme pWPS (více)</li>
									<li>Zajistíme požadovaný základní a přídavný materiál</li>
									<li>Zajistíme dozorující orgán: TUV SUD (www.tuev-nord.de/)</li>
									<li>Připravíme potřebnou dokumentaci</li>
									<li>Svaříme zkušební svarové spoje</li>
									<li>Vyhodnotíme svary: NDT, Mechanické a Metalografické zkoušky</li>
									<li>Inspekční orgán vystaví certifikáty WPQR</li>
								</ol>
							</div>',
	'row_main_text' => '<h2>Získání WPQR nikdy nebylo jednodušší a finančně dostupnější.</h2>
						<p>Požadované WPQR si objednáte elektronicky prostřednictvím e-mailu. Hotovou WPQR vystavenou na vaši společnost obdržíte fyzicky poštou a v elektronické podobě prostřednictvím email. <strong>Celý proces tvorby WPQR dozoruje mezinárodní dozorová organizace TÜV NORD. Veškerá dokumentace je v německém nebo anglickém jazyce (dle volby zákazníka).</strong></p>
						<h2>WPQR na klíč. Vy nemusíte dělat nic a šetříte čas i náklady.</h2>
						<p>My zařídíme pWPS, materiál, svaření zkušebních spojů, dozor, zkoušky a finální WPQR. Ušetříte pracovní sílu, čas i náklady.</p>
						<h2>Zkušební spoj si svaříte sami?</h2>
						<p>Ano i to je možné. Zkušební svarové spoje můžete dodat i hotové (Vámi svařené). V tomto případě zajistíme potřebné materiálové zkoušky, dozor TUV SUD a finální WPQR.</p>',
	
	'contact_form' =>  '<h2 class="form-header">Kontaktní formulář</h2>
						<div class="form-group">
							<label for="sel">Co vás zajímá?</label>
							<select multiple class="form-control" name="sel[]" id="sel" required>
								<option>WPQR na klíč</option>
								<option>WPQR - materiálové zkoušky svar. spoje</option>
								<option>Mechanické zkoušky</option>
								<option>Matematická analýza</option>
							</select>
						</div>
						<div class="form-group">
							<label for="message">Upřesněte svůj dotaz / poptávku</label>
							<textarea class="form-control" rows="2" name="message" id="message" placeholder="Napište vlastní text" required></textarea>
						</div>
						<div class="form-group">
							<label for="email">Váš email</label>
							<input type="email" class="form-control" name="email" id="email" placeholder="Napište váš email" required>
						</div>
						<button type="submit" class="btn btn-lg btn-block btn-orange">Odeslat dotaz</button>',

	'reference_title' => 'Reference',
	'reference' => 'Zakládáme si na 100% spokojenosti našich klientů.',
	'reference_row' => '<div class="row text-center hover-zoom-container">
							<div class="col-md-3 text-center">
								<img class="img-circle" src="../img/reference/reference_doosan.png" alt="" width="222" height="210">
								<div class="reference-text-box">
									<strong>DOOSAN</strong><br>
									Výrobce systémů turbogenerátorů
								</div>	
							</div>
							<div class="col-md-3">
								<img class="img-circle" src="../img/reference/reference_skoda.png" alt="" width="222" height="210">
								<div class="reference-text-box">
									<strong>ŠKODA TRANSPORTATION</strong><br>
									špička v oboru dopravního strojírenství
								</div>	
							</div>
							<div class="col-md-3">
								<img class="img-circle" src="../img/reference/reference_seele.png" alt="" width="222" height="210">
								<div class="reference-text-box">
									<strong>SEELE</strong><br>
									světový výrobce rozměrných svařovaných konstrukcí	
								</div>	
							</div>
							<div class="col-md-3">
								<img class="img-circle" src="../img/reference/reference_sanborn.png" alt="" width="222" height="210">
								<div class="reference-text-box">
									<strong>SANBORN</strong><br>
									světový výrobce dílů pro zařízení v energetice, petrochemii a dopravě
								</div>
							</div>
						</div>',

	'about_us_title' => 'O nás',

	'about_us' =>  '<p><strong>Společnost Výzkumný a zkušební ústav Plzeň s.r.o. byla založena r. 1907. VZÚ Plzeň se věnuje výzkumu, vývoji a akreditovanému zkušebnictví.</strong> Mezi nejdůležitejší činnosti společnosti patři:</p>
					<ul>
						<li>Výzkum a zkoušky zaměřené na zvyšování provozní spolehlivosti a životnosti energetických zařízení - vibrační diagnostika, snižování hlučnosti, materiálové zkoušky atd.</li>
						<li>Komplexní řešení problémů spojených s provozním zatížením, spolehlivostí a životností silničních a kolejových vozidel - PC simulace, zkoušky na zkušebně, měření v provozu.</li>
						<li>Akreditované zkoušky, kalibrace a měření pro široký okruh zákazníků. Zkoušení a analýza svarových spojů.</li>
						<li>Výpočty v oblasti pevnosti, dynamiky, únavového poškození, deformační odolnosti, aerodynamiky a termomechaniky.</li>
						<li>Výzkum a vývoj žárových nástřiků pro prvovýrobu i renovaci včetně jejich průmyslových aplikací.</li>
					</ul>',
					
	'company_name_header' => 'Výzkumný a zkušební ústav Plzeň s.r.o. - 110 LET (1907 - 2017)',

	'images_row' => '<div class="row images-row hover-zoom-container">
						<div class="images-row-col-8">
							<div><p><strong>WPQR</strong><br>Materiál, svařování<br>dozor, testování<br>dokumentace</p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_1.png" alt="" width="114" height="114">
						</div>
						<div class="images-row-col-8">
							<div><p><strong>Materiálová analýza</strong><br>Mechanické zkoušky<br>Metalografická analýza<br>Chemie</p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_2.png" alt="" width="114" height="114">
						</div>
						<div class="images-row-col-8">
							<div><p><strong>Tepelné<br>zpracování</strong></p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_3.png" alt="" width="114" height="114">
						</div>
						<div class="images-row-col-8">
							<div><p><strong>Aplikovaná<br>mechanika</strong></p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_4.png" alt="" width="114" height="114">
						</div>
						<div class="images-row-col-8">
							<div><p><strong>Žárové<br>nástřiky</strong></p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_5.png" alt="" width="114" height="114">
						</div>
						<div class="images-row-col-8">
							<div><p><strong>Dynamická<br>zkušebna</strong></p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_6.png" alt="" width="114" height="114">
						</div>
						<div class="images-row-col-8">
							<div><p><strong>Hluk a vibrace</strong></p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_7.png" alt="" width="114" height="114">
						</div>
						<div class="images-row-col-8">
							<div><p><strong>Kalibrace</strong></p></div>
							<img class="img-circle img-mini" src="../img/prefooter_img/prefooter_img_8.png" alt="" width="114" height="114">
						</div>
					 </div>'
	
);
?>