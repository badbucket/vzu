﻿<?php

define("PROTOCOL", "http://");
define("DOMAIN", "wpqr.cz");
define("ASSETS", DOMAIN."/assets");
define("IMAGES", ASSETS."/images");
define("DIST", ASSETS."/dist");
define("AUTHOR", "Jakub Skála");
define("TITLE", "VZÚ Plzeň");
define("DEFAULT_PAGE", "wpqr");
define("ERROR_PAGE", "wpqr");
define("CONTACT_EMAIL", "blamoid@gmail.com");
define("CONTACT_PERSON", "Jakub Skála");

// Language
// 
if(isset($_GET['lang'])){
	$lang = setLanguage($_GET['lang']);
	$_SESSION['lang'] = $lang;
	setcookie('lang', $lang, time()+(3600*24*30));
}else if(isset($_POST['lang'])){
	$lang = setLanguage($_POST['lang']);
	$_SESSION['lang'] = $lang;
	setcookie('lang', $lang, time()+(3600*24*30));
}else if(isset($_SESSION['lang'])){
	$lang = $_SESSION['lang'];
}else if(isset($_COOKIE['lang'])){
	$lang = $_COOKIE['lang'];
}else{
	$lang = 'cs';
}

// Templates - template, CZ, EN, DE
// 
// WPQR (na klíč) Materiálové zkoušky Žárové nástřiky Další služby Reference O nás Kontakt
$urls = array(
  'tpl' => array("wpqr", "sluzby", "reference", "o-nas", "kontakt"),
  'cs' => array("wpqr", "sluzby", "reference", "o-nas", "kontakt"),
  'de' => array("wpqr", "sluzby", "referenzen", "uber-uns", "kontakte")
);

// URL
// 
if(isset($_GET['url'])){
	$url = explode("/", cover($_GET['url']));
	$link = getTemplate($urls, $lang, $url, DEFAULT_PAGE, ERROR_PAGE);
}else{
	$link=DEFAULT_PAGE;
}

?>
