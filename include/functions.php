<?php

function cover($str){ // Funkce pro eleminovani nezadoucich znaku v retezcich ziskanych z formularu
	return htmlspecialchars($str);
}

function setLanguage($str){
	$supported = array("cs", "de");
	if (in_array($str, $supported)) {
		return $str;
	}
	return "cs";
}

function getTemplate($urls, $lang, $url, $default, $error){
	if(isset($url[0])){
		$key = exists($urls, $lang, $url[0]);
		if($key!=NULL){
			$link=$urls['tpl'][$key];
		}else{
			return $error;
		}
		if(isset($url[1])){
			$key = exists($urls, $lang, $url[1]);
			if($key!=NULL){
				$link.='/'.$urls['tpl'][$key];
			}else{
				return $error;
			}
		}
		return $link;
	}
	return $default;


}

function exists($urls, $lang, $url) {
	if (in_array($url, $urls[$lang])) {
		return array_search($url, $urls[$lang]);
	} else if (in_array($url, $urls['cs'])) {
		$lang='cs';
		return array_search($url, $urls['cs']);
	} else if (in_array($url, $urls['de'])) {
		$lang='de';
		return array_search($url, $urls['de']);
	}
	return NULL;
}

?>