<?php require_once('loader.php'); ?>
<!doctype html>
<html class="no-js" lang="cs">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	<title><?php echo TITLE; ?> | <?php echo $lg['title']; ?></title>
	<meta name="description" content="<?php echo $common['description']; ?>">
	<meta name="keywords" content="<?php echo $common['keywords']; ?>">
	<meta name="author" content="<?php echo AUTHOR; ?>">
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
	<meta name="robots" content="index, follow">
	
	<!--<link rel="stylesheet" href="../css/configurator.css?v=2" media="screen">-->
	<link rel="shortcut icon" href="../favicon.ico" />

	<!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Row with equal height columns hack -->
    <link href="../css/equal-height-columns.css" rel="stylesheet" />

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../js/ie-emulation-modes-warning.js"></script>

    <!--<link rel="stylesheet/less" href="../less/style.less">
    <script src="../js/libs/less-2.7.2.min.js"></script>-->
    <link rel="stylesheet" href="../css/style.css" media="screen">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!--[if lte IE 8]><script src="assets/dist/js/libs/html5shiv.min.js"></script><![endif]-->
</head>
<body id="d-body">

    <div class="container">    
        <div class="row row-top">    
            <div class="col-md-7 col-sm-12 logo-container">
                <div>
                    <a href="/"><img class="logo" src="../img/logo.png"/></a>
                </div>
            </div>
            <div class="col-md-2 col-sm-12 language-select-container">
                <form class="language-select" method="post" action="<?php echo $_SERVER['REDIRECT_URL'] ? $_SERVER['REDIRECT_URL'] : str_replace('index.php', '', $_SERVER['PHP_SELF']); ?>" id="form-language">
                    <input type="submit" name="lang" value="cs" id="lang-cs" onChange="this.form.submit();" <?php if ($lang == 'cs') echo ' class="language-selected"';?>>
                    <input type="submit" name="lang" value="de" id="lang-de" onChange="this.form.submit();" <?php if ($lang == 'de') echo ' class="language-selected"';?>>
                </form>
            </div>
            <div class="col-md-3 col-sm-12">
                <div notestyle="light_blue" class="top-contact-box note-div">
                    <table class="contact-table">
                        <tr>
                            <td>E-mail:</td>
                            <td>novak@vzuplzen.cz</td>
                        </tr>
                        <tr>
                            <td>Telefon:</td>
                            <td>+420 371 430 764</td>
                        </tr>
                    </table>
                </div>
            </div>   
	    </div>
    </div>

	<hr class="hr-blue-new">
  
    <nav class="navbar navbar-default navbar-vzu">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav text-uppercase invisible">
                    <li <?php if($urls['tpl'][0]==$link) echo ' class="active"';?> title="<?php echo $common['wpqr']; ?>">
                        <a href="<?php echo PROTOCOL.DOMAIN.'/'.$urls[$lang][0]; ?>">
                            <?php echo $common['wpqr']; ?>
                        </a>
                    </li>
                    <li <?php if($urls['tpl'][1]==$link) echo ' class="active"';?> title="<?php echo $common['sluzby']; ?>">
                        <a href="<?php echo PROTOCOL.DOMAIN.'/'.$urls[$lang][1]; ?>">
                            <?php echo $common['sluzby']; ?>
                        </a>
                    </li>
                    <li <?php if($urls['tpl'][2]==$link) echo ' class="active"';?> title="<?php echo $common['reference']; ?>">
                        <a href="<?php echo $urls[$lang][0] !== $link ? PROTOCOL.DOMAIN.'/'.$urls[$lang][0] : ''; ?>#reference">
                            <?php echo $common['reference']; ?>
                        </a>
                    </li>
                    <li <?php if($urls['tpl'][3]==$link) echo ' class="active"';?> title="<?php echo $common['o-nas']; ?>">
                        <a href="<?php echo $urls[$lang][0] !== $link ? PROTOCOL.DOMAIN.'/'.$urls[$lang][0] : ''; ?>#o-nas">
                            <?php echo $common['o-nas']; ?>
                        </a>
                    </li>
                    <li <?php if($urls['tpl'][4]==$link) echo ' class="active"';?> title="<?php echo $common['kontakt']; ?>">
                        <a href="<?php echo $urls[$lang][0] !== $link ? PROTOCOL.DOMAIN.'/'.$urls[$lang][0] : ''; ?>#kontakt">
                            <?php echo $common['kontakt']; ?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="carouselBackground">
        <div id="myCarousel" class="carousel fade" data-ride="carousel" data-interval="2700" data-pause="null">
            <div class="carousel-inner" role="listbox">
                <div class="item item-skipped">
                    <img src="../img/carousel_main/carousel_main.jpg" width="1107" height="215" usemap="#carouselPartialIndicatorsMap">
                </div>
                <div class="item active">
                    <img src="../img/carousel_main/carousel_main_1.jpg" alt="Mezinárodně akreditovaná laboratoř" width="1107" height="215" usemap="#carouselPartialIndicatorsMap">
                    <div class="banner-caption-container invisible">
                        <div class="banner-caption-table">
                            <p><?php echo $common['banner_text_znak']; ?></p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="../img/carousel_main/carousel_main_2.jpg" alt="Skvělý poměr kvalita/cena" width="1107" height="215" usemap="#carouselPartialIndicatorsMap">
                    <div class="banner-caption-container invisible">
                        <div class="banner-caption-table">
                            <p><?php echo $common['banner_text_ruka']; ?></p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="../img/carousel_main/carousel_main_3.jpg" alt="Ověřeno zákazníky z celého světa. Slavíme 110 let." width="1107" height="215" usemap="#carouselPartialIndicatorsMap">
                    <div class="banner-caption-container invisible">
                        <div class="banner-caption-table">
                            <p><?php echo $common['banner_text_lide']; ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <map name="carouselPartialIndicatorsMap" id="carouselImageMap">
                <area shape="rect" coords="0,0,563,215" data-target="#myCarousel" data-slide-to="">
                <area shape="rect" coords="564,0,744,215" data-target="#myCarousel" data-slide-to="1">
                <area shape="rect" coords="745,0,925,215" data-target="#myCarousel" data-slide-to="2">
                <area shape="rect" coords="926,0,1107,215" data-target="#myCarousel" data-slide-to="3">
            </map>
        </div>
    </div>
    <?php 
        if (isset($_GET['debugoff'])) { 
            $_SESSION['debug'] = false;  
        }  
        else if ($_SESSION['debug'] == true || isset($_GET['debug'])) { 
            $_SESSION['debug'] = true;  
            include('debuglog.php');  
        }  
    ?> 
