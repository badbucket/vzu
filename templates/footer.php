	<footer>
		<div class="container container-footer" id="kontakt">
			<div class="col-sm-5 text-right">
				<table>
					<tr>
						<td>Výzkumný a zkušební ústav Plzeň s.r.o.</td>
					</tr>
					<tr>
						<td>Tylova 1581/46, 301 00, Plzeň</td>
					</tr>
				</table>
			</div>
			<div class="col-sm-2 text-center">
				<table>
					<tr>
						<td>IČO: 47718684</td>
					</tr>
					<tr>
						<td>DIČ: CZ47718684</td>
					</tr>
				</table>
			</div>
			<div class="col-sm-5 text-left">
				<table class="contact-table-footer">
					<tr>
						<td>E-mail:</td>
						<td>novak@vzuplzen.cz</td>
					</tr>
					<tr>
						<td>Telefon:</td>
						<td>+420 371 430 764</td>
					</tr>
				</table>
			</div>
		</div>
	</footer>

    <script src="../js/ie10-viewport-bug-workaround.js"></script>

	<script src="../js/libs/jquery-3.2.1.min.js"></script>

	<script src="/js/libs/jquery.colorbox-min.js"></script>

	<script src="../js/bootstrap.min.js"></script>

	<script src="../js/libs/jquery.rwdImageMaps-1.6.min.js"></script>

	<script src="../js/libs/waitforimages.min.js"></script>

	<script src="../js/javascript.js"></script>

</body>
</html>
