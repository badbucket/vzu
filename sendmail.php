<?php
if (isset($_POST['email']) && isset($_POST['subject']) && isset($_POST['body'])) {
    $error = '';
    try {

        $headers = "From: " . strip_tags('dotaz@wpqr.cz') . "\r\n";
        $headers .= "Reply-To: ". strip_tags($_POST['email']) . "\r\n";
        $headers .= "BCC: wpqr@wpqr.cz\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        
        $to = "novak@vzuplzen.cz,adamek@vzuplzen.cz,info@wpqr.de";
        // $to = "blamoid@gmail.com,skalaja@seznam.cz";
        
        if ( mail($to, $_POST['subject'], $_POST['body'], $headers) ) {
            echo 'SUCCESS';
        }
        else { 
            echo 'FAIL';
        }
    } catch (Exception $error) {
        print_r($error);
    }
} else { 
    echo 'FAIL'; 
}
?>
